terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.87.0"
    }
  }

  # Storing terraform states in S3
  backend "s3" {
    endpoint       = ""
    bucket         = ""
    region         = ""
    key            = "terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
   }
}
