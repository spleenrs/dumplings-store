# Getting network information
module "yandex_cloud_network" {
  source = "./modules/tf-yc-network"
}

# Creating a Kubernetes cluster
module "yandex_cloud_k8s" {
  source     = "./modules/tf-yc-k8s-cluster"
  folder_id  = var.folder_id
  zone       = var.zone
  net_id     = module.yandex_cloud_network.yandex_vpc_nets.id
  subnet_id  = module.yandex_cloud_network.yandex_vpc_subnets.id
  cidr       = module.yandex_cloud_network.yandex_vpc_subnets.v4_cidr_blocks
  api_token  = var.api_token
  gitlab_uri = var.gitlab_uri
  id_project = var.id_project
}

# Creating a storage for images
module "yandex_cloud_s3" {
  source     = "./modules/tf-yc-s3"
  api_token  = var.api_token
  gitlab_uri = var.gitlab_uri
  id_project = var.id_project
}

# Creating a public dns zone
module "yandex_cloud_dns" {
  source     = "./modules/tf-yc-dns"
  folder_id  = var.folder_id
}
