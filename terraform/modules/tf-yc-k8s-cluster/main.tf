# A resource for creating a Kubernetes cluster in the Yandex Cloud. The resource name is "k8s-in-yc".
resource "yandex_kubernetes_cluster" "k8s-in-yc" {
  # Kubernetes cluster name
  name = var.cluster_name
  # The identifier of the VPC network on which the cluster will be deployed (default).
  network_id = var.net_id 
  # Configuration for cluster master nodes
  master {
    # Location of master nodes
    master_location {
      # The zone in which the control nodes will be placed
      zone      = var.zone
      # The subnet in which the control nodes will be placed
      subnet_id = var.subnet_id
    }
    public_ip = true
    # List of security group identifiers
    security_group_ids = [yandex_vpc_security_group.k8s-main-sg.id]
  }
  # Service account for cluster management
  service_account_id      = yandex_iam_service_account.k8s-sa.id
  # Service account for nodes management
  node_service_account_id = yandex_iam_service_account.k8s-sa.id
  depends_on = [
    yandex_resourcemanager_folder_iam_member.k8s-clusters-agent,
    yandex_resourcemanager_folder_iam_member.vpc-public-admin,
    yandex_resourcemanager_folder_iam_member.vpc-securityGroups-admin,
    yandex_resourcemanager_folder_iam_member.iam-serviceAccounts-user,
    yandex_resourcemanager_folder_iam_member.k8s-editor,
    yandex_resourcemanager_folder_iam_member.load-balancer-admin
  ]
}

# Creating a service account
resource "yandex_iam_service_account" "k8s-sa" {
  name        = "k8s"
  description = "K8S service account"
}

# The service account is assigned the role "k8s.clusters.agent"
resource "yandex_resourcemanager_folder_iam_member" "k8s-clusters-agent" {
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

# The service account is assigned the role "vpc.publicAdmin"
resource "yandex_resourcemanager_folder_iam_member" "vpc-public-admin" {
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

# The service account is assigned the role "vpc.securityGroups.admin"
resource "yandex_resourcemanager_folder_iam_member" "vpc-securityGroups-admin" {
  folder_id = var.folder_id
  role      = "vpc.securityGroups.admin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

# The service account is assigned the role "iam.serviceAccounts.user"
resource "yandex_resourcemanager_folder_iam_member" "iam-serviceAccounts-user" {
  folder_id = var.folder_id
  role      = "iam.serviceAccounts.user"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

# The service account is assigned the role "k8s.editor"
resource "yandex_resourcemanager_folder_iam_member" "k8s-editor" {
  folder_id = var.folder_id
  role      = "k8s.editor"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

# The service account is assigned the role "k8s.editor"
resource "yandex_resourcemanager_folder_iam_member" "load-balancer-admin" {
  folder_id = var.folder_id
  role      = "load-balancer.admin"
  member    = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
}

# Creating a config file for local operation of the kubectl utility
resource "null_resource" "kubeconf_start" {
  provisioner "local-exec" {
    command = "yc managed-kubernetes cluster get-credentials --id ${yandex_kubernetes_cluster.k8s-in-yc.id} --external"
 }
  depends_on = [
    yandex_kubernetes_cluster.k8s-in-yc
  ]
}

# Creating a static Kubernetes configuration file for use in Gitlab CI
resource "null_resource" "kubeconf_static" {
  provisioner "local-exec" {
    command = "${path.module}/create_kubeconf.sh ${var.folder_id} ${yandex_kubernetes_cluster.k8s-in-yc.id} ${var.cluster_name}"
  }
  depends_on = [
    null_resource.kubeconf_start
  ]
}

# The content of a kubeconfig file
data "local_file" "config_64" {
  filename = "${path.module}/${var.cluster_name}/kubeconfig"
  depends_on = [
    null_resource.kubeconf_static
  ]
}

# Setting the Gitlab variable KUBECONFIG
resource "null_resource" "set_config" {
  provisioner "local-exec" {
    command = "curl --request PUT --header 'PRIVATE-TOKEN: ${var.api_token}' 'https://${var.gitlab_uri}/api/v4/projects/${var.id_project}/variables/KUBERCONF' --form 'value=${data.local_file.config_64.content}'"
  }
  depends_on = [
    data.local_file.config_64
  ]
}
