# Creating a group of worker nodes
resource "yandex_kubernetes_node_group" "nodes-for-k8s" {
  # Managed Service for Kubernetes cluster ID
  cluster_id = yandex_kubernetes_cluster.k8s-in-yc.id
  # Managed Service for Kubernetes node group name
  name       = var.node_group_name
  # Managed Service for Kubernetes node parameters
  instance_template {
    # Node name template
    name       = "${var.node_names}-{instance.index}"
    # Managed Service for Kubernetes Node Platform
    platform_id = var.platform
    # Required resources
    resources {
      memory = var.memmory
      cores  = var.core
    }
    # Public IP address
    network_interface {
      subnet_ids = [var.subnet_id]
      nat        = true
    }
    # Network acceleration type (standard - no acceleration)
    network_acceleration_type = "standard"
    # Container runtime
    container_runtime {
     type = "containerd"
    }
  }
  # Scaling settings
  scale_policy {
    fixed_scale {
      # Number of nodes in the group
      size = var.size
    }
  }
}
