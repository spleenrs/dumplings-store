variable "cluster_name" {
  description = "Cluster name"
  default     = ""
  type        = string
}

variable "node_group_name" {
  description = "Node group name"
  default     = ""
  type        = string
}

variable "node_names" {
  description = "Template for node names"
  default     = ""
  type        = string
}

variable "net_id" {
  description = "Net id"
  type        = string
}

variable "subnet_id" {
  description = "Subnet id"
  type        = string
}

variable "cidr" {
  description = "V4 cidr blocks"
  type        = list(string)
}

variable "zone" {
  description = "Zone"
  type        = string
}

variable "core" {
  description = "Number of cores"
  default     = 2
  type        = number
}

variable "memmory" {
  description = "Volume of memmory"
  default     = 2
  type        = number
}

variable "platform" {
  description = "Platform id"
  default     = "standard-v1"
  type        = string
}

variable "size" {
  description = "Number of nodes in the group"
  default     = 2
  type        = number
}

variable "folder_id" {
  type        = string
  description = "Id folder"
}

variable "api_token" {
  description = "Gitlab token"
  type        = string
}

variable "gitlab_uri" {
  description = "Gitlab URL"
  type        = string
}

variable "id_project" {
  description = "Gitlab project id"
  type        = string
}
