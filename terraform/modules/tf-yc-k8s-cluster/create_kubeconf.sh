#!/bin/bash

# Creating a static kubeconfig file according to the instructions 
# https://yandex.cloud/ru/docs/managed-kubernetes/operations/connect/create-static-conf
FOLDER_ID=$1
CLUSTER_ID=$2
CLUSTER_NAME=$3

DIR=./modules/tf-yc-k8s-cluster/$CLUSTER_NAME

set -xe
echo $DIR
mkdir -p $DIR

yc --folder-id=$FOLDER_ID managed-kubernetes cluster get --id $CLUSTER_ID --format json | \
    jq -r .master.master_auth.cluster_ca_certificate | \
    awk '{gsub(/\\n/,"\n")}1' > $DIR/ca.pem

kubectl create -f ./modules/tf-yc-k8s-cluster/sa.yaml

SA_TOKEN=$(kubectl -n kube-system get secret $(kubectl -n kube-system get secret | \
    grep admin-user | \
    awk '{print $1}') -o json | \
    jq -r .data.token | \
    base64 --d)

MASTER_ENDPOINT=$(yc --folder-id=$FOLDER_ID managed-kubernetes cluster get --id $CLUSTER_ID \
    --format json | \
    jq -r .master.endpoints.external_v4_endpoint)

kubectl config set-cluster $CLUSTER_NAME \
               --certificate-authority=$DIR/ca.pem --embed-certs \
               --server=$MASTER_ENDPOINT \
               --kubeconfig=$DIR/config.yaml

kubectl config set-credentials admin-user \
                --token=$SA_TOKEN \
                --kubeconfig=$DIR/config.yaml

kubectl config set-context $CLUSTER_NAME \
               --cluster=$CLUSTER_NAME \
               --user=admin-user \
               --kubeconfig=$DIR/config.yaml

kubectl config use-context $CLUSTER_NAME \
               --kubeconfig=$DIR/config.yaml

base64 $DIR/config.yaml > $DIR/kubeconfig

rm $DIR/ca.pem
