# Creating a static access key
resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = var.sa_id
  description        = "static access key for object storage"
}

# Creating a bucket
resource "yandex_storage_bucket" "static" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = var.bucket_name
  max_size   = var.max_size
  # Open public access
  anonymous_access_flags {
    read        = true
    list        = true
    config_read = false
  }
}

# List of files to upload to the bucket
locals {
  files_to_upload = toset(fileset(var.path_to_files, "*"))
}

# Uploading all files to a bucket
resource "yandex_storage_object" "uploaded_objects" {
  for_each  = { for idx, file in local.files_to_upload : idx => file }

  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = yandex_storage_bucket.static.bucket
  key        = each.value
  source     = format("%s%s", var.path_to_files, each.value)
  depends_on = [
    yandex_storage_bucket.static
  ]
}

# Setting the Gitlab variable LINK_IMAGE
resource "null_resource" "set_link" {
  provisioner "local-exec" {
    command = "curl --request PUT --header 'PRIVATE-TOKEN: ${var.api_token}' 'https://${var.gitlab_uri}/api/v4/projects/${var.id_project}/variables/LINK_IMAGE' --form 'value=https://storage.yandexcloud.net/${yandex_storage_bucket.static.bucket}/'" 
  }
  depends_on = [
    yandex_storage_bucket.static
  ]
}
