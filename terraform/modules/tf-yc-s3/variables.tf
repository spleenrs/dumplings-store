variable "sa_id" {
  description = "Service account id"
  default     = ""
  type        = string
}

variable "bucket_name" {
  description = "Bucket name"
  default     = ""
  type        = string
}

variable "max_size" {
  description = "Bucket size"
  default     = "104857600"
  type        = string
}

variable "path_to_files" {
  description = "Path to images"
  default     = "modules/tf-yc-s3/img/"
  type        = string
}

variable "api_token" {
  description = "Gitlab token"
  type        = string
}

variable "gitlab_uri" {
  description = "Gitlab URL"
  type        = string
}

variable "id_project" {
  description = "Gitlab project id"
  type        = string
}
