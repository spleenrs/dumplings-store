# Creation of a public DNS zone
resource "yandex_dns_zone" "public-zone" {
  name      = "public-zone"
  folder_id = var.folder_id
  zone      = var.zone_name
  public    = true
}
