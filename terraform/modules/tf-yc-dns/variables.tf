variable "folder_id" {
  type        = string
  description = "Id folder"
}

variable "zone_name" {
  type        = string
  description = "Domain name"
  default     = "your.domain.com."
}
