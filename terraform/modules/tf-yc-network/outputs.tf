output "yandex_vpc_nets" {
  description = "Yandex.Cloud net"
  value       = data.yandex_vpc_network.net
}

output "yandex_vpc_subnets" {
  description = "Yandex.Cloud subnet"
  value       = data.yandex_vpc_subnet.subnet
}
