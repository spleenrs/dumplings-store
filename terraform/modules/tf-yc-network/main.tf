# Information about a network
data "yandex_vpc_network" "net" {
  name = "${var.network_name}"
}

# Information about a subnet
data "yandex_vpc_subnet" "subnet" {
  name = "${data.yandex_vpc_network.net.name}-${var.subnet_name}"
}
