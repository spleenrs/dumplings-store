variable "network_name" {
  description = "Yandex.Cloud network name"
  type        = string
  default     = ""
}

variable "subnet_name" {
  description = "Yandex.Cloud subnet name"
  type        = string
  default     = ""
}
