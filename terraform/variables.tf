variable "zone" {
  type        = string
  description = "Zone name"
}

variable "cloud_id" {
  type        = string
  description = "Id cloud"
}

variable "folder_id" {
  type        = string
  description = "Id folder"
}

variable "api_token" {
  description = "Gitlab token"
  type        = string
}

variable "gitlab_uri" {
  description = "Gitlab URL"
  type        = string
}

variable "id_project" {
  description = "Gitlab project id"
  type        = string
}
