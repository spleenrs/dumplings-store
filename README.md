# Dumplings store

<img width="600" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Описание

Реализован полный цикл сборки и поставки микросервисного приложения интернет-магазина (backend - Golang, frontend - Vue.js) в кластер Kubernetes Yandex Cloud (Managed Service for Kubernetes).
Инфраструктура для деплоя приложения создается с помощью **Terraform** (Kubernetes-кластер, S3 для  изображений, публичная зона DNS).
Публикация приложения в кластер **Kubernetes** производится через **Helm-чарты** с использованием возможностей **Gitlab CI/CD**.

**Структура:**

```
├── backend
│   ├── cmd
│   ├── Dockerfile
│   ├── go.mod
│   ├── go.sum
│   └── internal
├── frontend
│   ├── babel.config.js
│   ├── Dockerfile
│   ├── package.json
│   ├── package-lock.json
│   ├── public
│   ├── src
│   ├── tsconfig.json
│   └── vue.config.js
├── helm
│   └── dumplings-chart
├── README.md
└── terraform
    ├── main.tf
    ├── modules
    ├── outputs.tf
    ├── provider.tf
    ├── terraform.tfvars
    ├── variables.tf
    └── version.tf
```

## Установка

**Инфраструктура:**

Перед началом создания инфраструктуры необходимо установить:

- Интерфейс командной строки Yandex Cloud **yc** [инструкция](https://yandex.cloud/ru/docs/cli/quickstart#install)
- Утилиту потоковой обработки JSON-файлов **jq** `sudo apt install jq`
- Интерфейс командной строки Kubernetes **kubectl** [инструкция](https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/)
- Утилиту командной строки для отправки HTTP-запросов **curl** `sudo apt install curl`
- Инструмент для создания облачной инфраструктуры **Terraform v 1.5.7** [инструкция](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-quickstart#install-terraform)

Войдите в [консоль управления Yandex](https://console.yandex.cloud/), если у вас нет каталога, [создайте](https://yandex.cloud/ru/docs/resource-manager/operations/folder/create) его. Если у вас нет сети, [создайте](https://yandex.cloud/ru/docs/vpc/operations/network-create) ее и [подсети](https://yandex.cloud/ru/docs/vpc/operations/subnet-create). 

Создайте в каталоге сервисный аккаунт для установки кластера Kubernetes.
- Создайте iam ключ для указанного сервисного аккаунта:
  ```yc iam key create \
    --service-account-id <id аккаунта> \
    --folder-name <имя каталога> \
    --output key.json```
- Создайте профиль:
  ```yc config profile create <имя профиля>```
- Установите созданный ключ для профиля:
  ```yc config set service-account-key key.json```
- Установите переменную окружения **(необходима для работы Terraform)**:
  ```export YC_TOKEN=$(yc iam create-token)```

Создайте в каталоге сервисный аккаунт для управления хранилищем S3 с ролью `storage.editor`. В консоле управления Yandex создайте статический ключ для данного аккаунта. Установите следующие переменные окружения:
- `export AWS_ACCESS_KEY_ID=<идентификатор статического ключа>`
- `export AWS_SECRET_ACCESS_KEY=<секретный ключ>`
Создайте приватный бакет с ограниченным доступом в Yandex Cloud Object Storage для хранения стейтов Terraform.
Установите значение поля `bucket` в корневом файле `version.tf` наименованием созданного бакета.

Установите следующие переменные окружения:
- `export TF_VAR_api_token=<Personal Access Token со Scope API в Gitlab>`
- `export TF_VAR_gitlab_uri=<Gitlab URL>`
- `export TF_VAR_id_project=<ID проекта в Gitlab>`

Создайте следующие переменные в настройках CI/CD Gitlab:
 - `KUBERCONF` - в эту переменную Terraform запишет статическую конфигурацию kubeconfig, закодированную base64.
 - `LINK_IMAGE` - в эту переменную Terraform запишет адрес созданного хранилища S3 для изображений сайта.

Измените значения переменных в файле `terraform.tfvars` и файлах `variables.tf` каждого модуля на требуемые вам значения (имена создаваемых ресурсов, id сервисных аккаунтов, количество ресурсов и т.д.)

В каталоге terraform выполните команду `terraform init` для инициализации провайдера и модулей. 
В каталоге terraform выполните команду `terraform apply` для создания инфраструктуры.

После выполнения указанных действий в Yandex Cloud будет развернут кластер Kubernetes c рабочими нодами и внешними ip-адресами и политиками безопасности, S3 хранилище с загруженными  в него изображениями для работы приложения, публичная зона DNS, а так же файл конфигурации `kubeconfig` для работы утилиты `kubectl` в каталоге ~\.kube\config. 
В переменных CI/CD Gitlab `KUBERCONF` и `LINK_IMAGE` установятся соответствующие значения.

**Приложение**

Создайте следующие переменные в настройках CI/CD Gitlab:
- `VERSION` - версия приложения (для версионирования docker-образов) в формате MAJOR.MINOR, значение PATCH заполнится автоматически значением CI_PIPELINE_ID.
- `HELM_VERSION` - версия helm-чарта, загружаемая в хранилище Nexus в формате MAJOR.MINOR.PATCH
- `F_TAG` - автоматически заполняется пайплайном сборки docker-образа фронтенда. Используется в пайплайне сборки и релиза helm-чарта.
- `B_TAG` - автоматически заполняется пайплайном сборки docker-образа бэкенда. Используется в пайплайне сборки и релиза helm-чарта.
- `NEXUS_LOGIN` - логин для подключения к хранилищу Nexus. 
- `NEXUS_PASS` - пароль для подключения к хранилищу Nexus.
- `NEXUS_URI` - URL хранилища Nexus.
- `NEXUS_REPO` - имя репозитория для хранения helm-чартов.
- `API_TOKEN` - Personal Access Token со Scope API в Gitlab.
- `GITLAB_URI` - Gitlab URL.
- `ID_PROJECT` - ID проекта в Gitlab.
- `DOCKERCONFIG` - учетные данные для подключения к Gitlab Container Registry (Mask variable).
- `DOMAIN` - джоменное имя, по которому будет доступно приложение.

Вы можете настроить параметры приложения, такие как количество реплик, доменное имя и т.д. в файле helm/dumplings-chart/values.yaml.
Установите пароль для доступа  к grafana в файле helm/grafana-values.yaml (adminPassword: "YourPassword").
Автоматический запуск пайплайнов сборки и релиза фронтенда и бэкенда происходит при изменениях в директориях `frontend` и `backend` соответственно.
Автоматический запуск пайплайна сборки helm-чарта и деплоя приложения в кластер Kubernetes происходит при изменениях в директории `helm` или при изменениях в одной из директорий `frontend` и `backend`.

После выполнения всех пайплайнов:
- Docker-образы фронтенда и бэкенда сохранятся в `Gitlab Container Registry` с заданными тэгами.
- Helm-чарт приложения с указанной версией сохранится в хранилище `Nexus`.
- В кластер Kubernetes установится `Cert-Manager` для управления сертификатами кластера от Let’s Encrypt.
- В кластер Kubernetes установится `Ingress-контроллер NGINX` для предоставления доступа к приложению из сети Интернет и работы TLS.
- В кластер Kubernetes установится приложение dumplings-store (https://DOMAIN).
- В кластер Kubernetes установится loki-stack для хранения и отображения логов.
- В кластер Kubernetes установится kube-prometheus-stack для мониторинга состояния приложения и кластера.
- В кластер Kubernetes установится grafana для визуализации информации от систем логирования и мониторинга. Автоматически добавятся источники данных Loki и Prometheus, а так же dashboard Loki Kubernetes Logs для просмотра логов приложения (https://grafana.DOMAIN).

После установки приложения в кластер:
- Выполните команду `kubectl get service/ingress-nginx-controller` для выяснения внешнего адреса балансировщика.
- В консоле управления Yandex в созданной публичной зоне DNS создайте А-запись для сопоставления внешнего адреса балансировщика с адресом домена.
- В консоле управления Yandex в созданной публичной зоне DNS создайте А-запись для сопоставления внешнего адреса балансировщика с адресом поддомена для доступа к grafana (grafana.DOMAIN)
- Импортируйте dashboard Dumplings из файла helm/dashboard.json в grafana для просмотра состояния приложения.























